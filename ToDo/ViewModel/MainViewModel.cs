﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using ToDo.Annotations;

namespace ToDo.ViewModel
{
	class MainViewModel: INotifyPropertyChanged 
	{ 

        public MainViewModel()
        {
            _newDo = "";
            _colection = new Model.Model();
            _select = 0;
            _readydecoration = "";
        }

        private string _newDo;
        private Model.Model _colection;
        private int _select;
        private string _readydecoration;

        public string ReadyDecaration
        {
	        get
	        {
		        return _readydecoration;
	        }
	        set
	        {
		        _readydecoration = value;
                OnPropertyChanged(nameof(ReadyDecaration));
	        }
        }
        public int Select
        {
	        get { return _select;}
	        set
	        {
		        _select = value;
                OnPropertyChanged(nameof(Select));
	        }
        }
        public Model.Model Colection
        {
	        get   { return _colection; }
	        set
	        {
		        _colection = value; OnPropertyChanged(nameof(Colection)); }
        }

        public string NewDo { get { return _newDo; } set { _newDo = value; OnPropertyChanged(nameof(NewDo)); } }

        private ICommand _adddoCommand;
        private ICommand _deldoCommand;
        private ICommand _editdoCommand;
        private ICommand _readyCommand;

        private IMultiValueConverter _readyConverter = new ReadyConverter();

        public ICommand DelDoCommand { get
        {
	        return _deldoCommand ??
	               (_deldoCommand = new RelayCommand<int>(DelDoCommandExe, x => x < Colection.Tasks.Count() && x >= 0)); } }
        public ICommand AddDoCommand { get { return _adddoCommand ?? 
                        (_adddoCommand = new RelayCommand<string>(AddDoCommandExe, x => !string.IsNullOrEmpty(x))); } }
        public ICommand EditDoCommand { get { return _editdoCommand ?? 
                        (_editdoCommand = new RelayCommand<string>(EditDoCommandExe, x => !string.IsNullOrEmpty(x)));
        } }
        public ICommand ReadyCommand { get { return _readyCommand ?? 
                        (_readyCommand = new RelayCommand<Model.Task>(ReadyDoCommandExe, x => x != null)); } }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void EditDoCommandExe(string obj)
        {
	        Colection.EditTaskById(Select, NewDo);
	        NewDo = " ";
        }
        private void ReadyDoCommandExe(Model.Task obj)
        {
            Colection.OnReadyChanged();
        }
        private void AddDoCommandExe(string obj)
        {
            Colection.AddTask(obj);
            NewDo = "";
        }
        private void DelDoCommandExe(int obj)
        {
            Colection.RemoveTaskById(obj);
        }

        public IMultiValueConverter ReadyConverter
        {
	        get { return _readyConverter; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

	public class ReadyConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			return values.Clone();
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;
        private Func<Model.Task, bool> p;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public RelayCommand(Action<int> delDoCommandExe, Func<Model.Task, bool> p)
        {
            this.p = p;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}
