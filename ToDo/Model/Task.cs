﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToDo.Annotations;
using ToDo.ViewModel;

namespace ToDo.Model
{
	public class Task: INotifyPropertyChanged
	{

		private string _taskName;
		private bool _ready;
		private int _id;

		public int id
		{
			get => _id;
			set 
			{
				if (_id == value) return;
				_id = value;
				OnPropertyChanged(nameof(TaskName));
			}
		}

		public string TaskName
		{
			get => _taskName;
			set
			{
				if (value == _taskName) return;
				_taskName = value;
				OnPropertyChanged(nameof(TaskName));
			}
		}

		public bool Ready
		{
			get => _ready;
			set
			{
				if (value == _ready) return;
				_ready = value;
				OnPropertyChanged(nameof(Ready));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
