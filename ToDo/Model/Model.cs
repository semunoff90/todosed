﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using ToDo.Annotations;
using ToDo.ViewModel;

namespace ToDo.Model
{
	public class ApplicationContext : DbContext
	{
		public ApplicationContext() : base("DefaultConnection")
		{
		}
		public DbSet<Task> Tasks { get; set; }
	}

	class Model
	{

		public Model()
		{
			db = new ApplicationContext();
			db.Tasks.Load();
			Tasks = db.Tasks.Local;
		}


		ApplicationContext db;

		public ObservableCollection<Task> Tasks { get; private set; }

		public void AddTask(string value)
		{
			Task newT = new Task();
			newT.TaskName = value;
			newT.Ready = false;
			Tasks.Add(newT);
			db.SaveChanges();
		}

		public void RemoveTaskById(int id)
		{
			if(Tasks.Count() < id || id < 0)
				return;
			Tasks.RemoveAt(id);
			db.SaveChanges();
		}

		public Model EditTaskById(int id, string newname)
		{
			if (Tasks.Count() < id || id < 0)
				return this;
			Tasks.ElementAt(id).TaskName = newname;
			db.SaveChanges();
			return this;
		}

		public void OnReadyChanged()
		{
			db.SaveChanges();
		}
	}
}
